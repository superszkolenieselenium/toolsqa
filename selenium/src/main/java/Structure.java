import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Structure {
    private String name;
    private String country;
    private String city;
    private String height;
    private String built;

    public Structure(){

    }
    public Structure(String name, String country, String city, String height, String built) {
        this.name = name;
        this.country = country;
        this.city = city;
        this.height = height;
        this.built = built;
    }
    public Structure(WebElement row){
        List<WebElement> cols = row.findElements(By.cssSelector("td"));
        name = row.findElement(By.cssSelector("th")).getText();
        country = cols.get(0).getText();
        city = cols.get(1).getText();
        height = cols.get(2).getText();
        built = cols.get(3).getText();
    }

    public void print(){
        System.out.println("Building " + name + " in " + city + " has " + height);
    }
}
