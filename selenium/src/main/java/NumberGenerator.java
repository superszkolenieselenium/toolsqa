import java.util.Random;

public class NumberGenerator {
    private Random generator = new Random();

    public int randomize(int i) {
        //losowanie liczby z zakresu 0-i
        int rndNumber = generator.nextInt(i);
        System.out.println("Randomed number: " + rndNumber);
        return rndNumber;
    }
}
