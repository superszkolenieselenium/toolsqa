import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;
import java.util.Random;

public class FormTest extends BaseTest {
    NumberGenerator numberGenerator = new NumberGenerator();

    @Test
    public void firstTest() throws InterruptedException {
        driver.get("http://toolsqa.com/automation-practice-form/");

        //imie
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.sendKeys("Jan");

        //nazwisko
        WebElement lastName = driver.findElement(By.name("lastname"));
        lastName.sendKeys("Kowalski");

        //płeć
        List<WebElement> sex = driver.findElements(By.name("sex"));
        sex.get(0).click();
        Assert.assertTrue(sex.get(0).isSelected());

        //lata doświadczenia
        int i = numberGenerator.randomize(6);
        List<WebElement> exp = driver.findElements(By.name("exp"));
        exp.get(i).click();
        Assert.assertTrue(exp.get(i).isSelected());

        //data
        WebElement date = driver.findElement(By.id("datepicker"));
        date.sendKeys("12.12.2018");

        //dodawanie pliku
        WebElement uploadPhoto = driver.findElement(By.id("photo"));

        File photo = new File("src\\files\\face.png");
        String absolutePathToPhoto = photo.getAbsolutePath();
        System.out.println("absolut path: " + absolutePathToPhoto);
        uploadPhoto.sendKeys(absolutePathToPhoto);

        //toole checkboxu
        List<WebElement> tools = driver.findElements(By.name("tool"));
        tools.get(0).click();
        tools.get(1).click();

        //tworzenie obiektu Select z obiektu WebElement
        WebElement continents = driver.findElement(By.id("continents"));
        Select countrySelect = new Select(continents);

        //pobranie ilości dostępnych opcji
        int numberOfOptions = countrySelect.getOptions().size();

        //wylosowanie liczby z zakresu 0 do liczbaOpcji-1
        i = numberGenerator.randomize(numberOfOptions - 1);

        //kliknięcie opcji o wybranym indeksie
        countrySelect.selectByIndex(i);


        //wybieranie dwóch 'selenium commands' po ich tekście
        WebElement commands = driver.findElement(By.id("selenium_commands"));
        Select commandsSelect = new Select(commands);
        commandsSelect.selectByVisibleText("Browser Commands");
        commandsSelect.selectByVisibleText("Wait Commands");


        //kliknięcie submit
        WebElement submit = driver.findElement(By.name("submit"));
        submit.click();

        //czekanie 3 sekundy
        Thread.sleep(3000);
    }
}