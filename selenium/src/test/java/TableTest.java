import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TableTest extends BaseTest {
    @BeforeMethod
    public void prepareTest() {
        driver.get("http://toolsqa.com/automation-practice-table/");
    }

    @Test
    public void createStructureHardcoded() {
        List<WebElement> allRows = driver.findElements(By.cssSelector("tbody tr"));
        WebElement firstRow = allRows.get(0);
        String name = firstRow.findElement(By.cssSelector("th")).getText();

        List<WebElement> cols = firstRow.findElements(By.cssSelector("td"));
        String country = cols.get(0).getText();
        String city = cols.get(1).getText();
        String height = cols.get(2).getText();
        String built = cols.get(3).getText();

        Structure structure = new Structure(name, country, city, height, built);
        structure.print();
    }

    @Test
    public void createStructure() {
        List<WebElement> allRows = driver.findElements(By.cssSelector("tbody tr"));
        WebElement firstRow = allRows.get(0);
        Structure structure = new Structure(firstRow);
        structure.print();
    }

    @Test
    public void createAllStructures() {
        List<Structure> structures = new ArrayList<Structure>();
        List<WebElement> allRows = driver.findElements(By.cssSelector("tbody tr"));

        for (int i = 0; i < allRows.size(); i++) {
            WebElement row = allRows.get(i);
            structures.add(new Structure(row));
        }

        for (int i = 0; i < structures.size(); i++) {
            structures.get(i).print();
        }
    }

    @Test
    public void createAllStructuresWithForeach() {
        List<Structure> structures = new ArrayList<Structure>();
        List<WebElement> allRows = driver.findElements(By.cssSelector("tbody tr"));

        for (WebElement row : allRows) {
            structures.add(new Structure(row));
        }

        for (Structure structure : structures) {
            structure.print();
        }
    }
}
